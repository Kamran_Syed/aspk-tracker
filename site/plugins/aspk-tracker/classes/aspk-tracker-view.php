<?php
if ( !class_exists('Aspk_Track_View')){
	class Aspk_Track_View{
		
		function __construct(){
			
			
		}
		
		function show_menu(){
			$css_class='selected_tab';
			$page_link = get_permalink();
			if(isset($_GET['aspk_page'])){
				$current_page = $_GET['aspk_page'];
			}
			?>
			<div class="tw-bs container">
				<div class="row">
					<div class="digital_tab col-md-2 <?php if($current_page=='track_add' || !isset($_GET['aspk_page']) ){ echo $css_class; }?>"> <a href="<?php echo $page_link.'?aspk_page=track_add';?>">Add User</a>
					</div>
					<div class="digital_tab col-md-2 <?php if($current_page=='track_view'  ){ echo $css_class; }?>"> <a href="<?php echo $page_link.'?aspk_page=track_view';?>">View Users</a>
					</div>
				</div>
			</div>
			<?php
		}
		
		function add_view_users($all_users){
			$page_link = get_permalink();
			$page = get_page_by_title( 'Show Points' );
			$show_points = get_permalink($page->ID);
			?>
			<div class="tw-bs container"><!-- start container -->
				<?php
				if(isset($_GET['aspk_page'])){
					$current_page = $_GET['aspk_page'];
					if($current_page == 'track_add'){
						?>
						<form method="post" action="">
							<div class="row aspk_clear" >
								<div class="col-md-2 aspk_top" ><b class="aspk_font">Name</b></div>
								<div class="col-md-9 aspk_top"><input type="text" required name="aspk_name_usr" ></div>
							</div>
							<div class="row aspk_clear">
								<div class="col-md-9 aspk_top"><input type="submit" name="aspk_add_usr" value="Add User" class="btn btn-primary"></div>
							</div>
						</form>
						<?php
					}else{
						if(count($all_users) < 1){
							?>
								<div class="row">
									<div class="col-md-12">
										<h2 class="aspk_center">Users not exists </h2>
									</div>
								</div>
							<?php
						}else{
							?>
							<div class="row aspk_clear" >
								<div class="col-md-12 aspk_top"><h2 class="aspk_center">View Users</h2></div>
							</div>
							<div class="row aspk_clear" >
								<div class="col-md-3 aspk_top"><b class="aspk_font">Name</b></div>
								<div class="col-md-4 aspk_top"><b class="aspk_font">URL</b></div>
								<div class="col-md-2 aspk_top"><b class="aspk_font">Delete</b></div>
								<div class="col-md-3 aspk_top"><b class="aspk_font">View Live</b>
								</div>
							</div>
						<?php
							foreach($all_users as $user){
								?>
								<div class="row aspk_clear" >
									<div class="col-md-3 aspk_top"><?php echo $user->name; ?></div>
									<div class="col-md-4 aspk_top"><?php echo admin_url( 'admin-ajax.php' ).'?key='.$user->key .'&uid='.$user->key2 .'&action=aspklistener';?></div>
									<div class="col-md-2 aspk_top"><a href="<?php echo $page_link.'?aspk_page=track_view&aspk_delete='.$user->key;?>"><img class="aspk_img" src="<?php echo plugins_url('images/cross.jpg', __FILE__);?>"></a></div>
									<div class="col-md-3 aspk_top"><a href="<?php echo $show_points.'?key='.$user->key;?>">View Live</a>
									</div>
								</div>
							<?php
							}
						}
					}
				}else{
					?>
					<form method="post" action="">
						<div class="row aspk_clear" >
							<div class="col-md-2 aspk_top"><b class="aspk_font">Name</b></div>
							<div class="col-md-9 aspk_top"><input type="text" required name="aspk_name_usr" ></div>
						</div>
						<div class="row aspk_clear" >
							<div class="col-md-9 aspk_top"><input type="submit" class="btn btn-primary" name="aspk_add_usr" value="Add User" ></div>
						</div>
					</form>
					<?php
				}
				?>
			</div><!-- end container -->
			<?php
		}
		
	}// calss ends
}//if ends