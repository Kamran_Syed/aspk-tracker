<?php
if ( !class_exists('Aspk_Track_Model')){
	class Aspk_Track_Model{
		
		function __construct(){
			
			
		}
		
		function install_model(){
			global $wpdb;
				
			$sql = "CREATE TABLE IF NOT EXISTS  `".$wpdb->prefix."aspk_tracker_user` ( 
				`key` VARCHAR( 10 ) NOT NULL PRIMARY KEY,
				`key2` VARCHAR( 10 ) NOT NULL ,
				`userid` INT( 11 ) NULL, 
				`name` VARCHAR( 40 ) NOT NULL 
				) ENGINE = InnoDB;";
			$wpdb->query($sql);
			
			$sql = "CREATE TABLE IF NOT EXISTS  `".$wpdb->prefix."aspk_hold_point_data` ( 
				`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`date_time` DATE  NULL,
				`key` VARCHAR( 10 ) NOT NULL,
				`name` VARCHAR( 40 ) NOT NULL ,
				`lon` VARCHAR( 30 ) NOT NULL ,
				`lat` VARCHAR( 30 ) NOT NULL 
				) ENGINE = InnoDB;";
			$wpdb->query($sql);
		}
		
		function add_users($key,$key_2,$userid,$name){
			global $wpdb;
			
			$sql = "INSERT INTO {$wpdb->prefix}aspk_tracker_user (`key`,`key2`,`userid`,`name`)  VALUES('{$key}','{$key_2}','{$userid}','{$name}')";
			$wpdb->query($sql);	
		}
		
		function get_all_users_by_uid($id){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}aspk_tracker_user WHERE userid={$id}";
			return $wpdb->get_results($sql);
		}
		
		function get_key_from_tracker($key){
			global $wpdb;
			
			$sql = "SELECT `key` FROM {$wpdb->prefix}aspk_tracker_user WHERE `key` = '{$key}'";
			return $wpdb->get_var($sql);
		}
		
		function get_detail_from_pont_table($key){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}aspk_hold_point_data WHERE `key` = '{$key}' Order by id ASC LIMIT 40";
			return $wpdb->get_results($sql);
		}
		
		function delete_user($key){
			global $wpdb;
			
			$sql = "DELETE FROM `".$wpdb->prefix."aspk_tracker_user` WHERE `key` ='{$key}'";
			$wpdb->query($sql);
		}
		
		
	}// calss ends
}//if ends