<?php

/**
 * Plugin Name: Aspk Tracker
 * Plugin URI: 
 * Description: Agile solutions Pk Tracker.
 * Version: 1.0
 * Author: Agile Solutions PK.
 * Author URI: http://agilesolutionspk.com
 */

require_once(__DIR__ ."/classes/aspk-tracker-view.php");
require_once(__DIR__ ."/classes/aspk-tracker-model.php");

if ( !class_exists('Aspk_Tracker_Solution')){
	class Aspk_Tracker_Solution{
		
		function __construct(){
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') );
			add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') ); 
			add_shortcode( 'aspk_trackers', array(&$this,'agile_track' ));
			add_action('init', array(&$this, 'fe_init'));
			add_shortcode( 'aspk_trackers_points', array(&$this,'agile_points' ));
		}
		
		function agile_points(){
			$trac_model = new Aspk_Track_Model();
			
			if(isset($_GET['key'])){
				$key = $_GET['key'];
				$results = $trac_model->get_detail_from_pont_table($key);
			}
			
			$points_array = array();
			foreach($results as $location){
				$x = new stdClass();
				$x->lon = $location->lon;
				$x->lat = $location->lat;
				$x->dt =$location->date_time;
				$points_array[] = $x;
			}
			$ret = $points_array;

			$data = json_encode($ret);
			?>
			<div class="tw-bs container"><!-- start container -->
				<div class="row aspk_clear">
					<div class="col-md-12 aspk_top" id="map123" style="height:400px;500px;"></div>
				</div>
				<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
				
				<script >
				
					function show_markers_points(){
							var data = <?php echo $data; ?>;
							var mapCanvas = document.getElementById('map123');
								var mapOptions = {
								  zoom: 8,
								  zoomControl: true,
								  center: new google.maps.LatLng( <?php echo $location->lat; ?>, <?php echo $location->lon; ?> ),
								  mapTypeId: google.maps.MapTypeId.ROADMAP
								}
								var map = new google.maps.Map(mapCanvas, mapOptions);
									for(var index in data) { 
										var pt = data[index];
										
										var location = new google.maps.LatLng(  pt['lat'],pt['lon'] );
										var marker = new google.maps.Marker({
											position: location,
											map: map,
											title: pt['dt']
										});
									}
					}
					google.maps.event.addDomListener(window, 'load', show_markers_points);
					
					
					
					
				</script>

					
			</div><!-- end container -->
			<?php
		}
		
		function fe_init(){
			
		}
		
		function install(){
			$track_model = new Aspk_Track_Model();
			
			$track_model->install_model();
			
		}
		
		function agile_track(){
			$track_v = new Aspk_Track_View();
			$track_model = new Aspk_Track_Model();
			
			if(isset($_GET['aspk_delete'])){
				$key = $_GET['aspk_delete'];
				$track_model->delete_user($key);
			}
			
			$uid = get_current_user_id();
			if(isset($_POST['aspk_add_usr'])){
				$name = $_POST['aspk_name_usr'];
				$key = $this->randomPassword_key();
				$key_2 = $this->randomPassword_key2();
				$key_exist = $track_model->get_key_from_tracker($key);
				if(!empty($key_exist)){
					$key = $this->randomPassword_key();
					$track_model->add_users($key,$key_2,$uid,$name);
				}else{
					$track_model->add_users($key,$key_2,$uid,$name);
				}
			}
			
			$all_users = $track_model->get_all_users_by_uid($uid);
			$track_v->show_menu();
			$track_v->add_view_users($all_users);
		}
		
		function randomPassword_key() {
			 $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789%#*^@";
			 $pass = array(); //remember to declare $pass as an array
			 $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			 for ($i = 0; $i < 10; $i++) {
				 $n = rand(0, $alphaLength);
				 $pass[] = $alphabet[$n];
			 }
			 return implode($pass); //turn the array into a string
		}
		
		function randomPassword_key2() {
			 $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
			 $pass = array(); //remember to declare $pass as an array
			 $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			 for ($i = 0; $i < 10; $i++) {
				 $n = rand(0, $alphaLength);
				 $pass[] = $alphabet[$n];
			 }
			 return implode($pass); //turn the array into a string
		}
		
		//it is a wordpress function 
		function wp_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('js-fe-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__));
			wp_enqueue_style( 'agile-fe-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
			wp_enqueue_style( 'agile-aspk-track', plugins_url('css/track.css', __FILE__) );
		}
		
	}// calss ends
}//if ends

new Aspk_Tracker_Solution();